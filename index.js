/*Load the epxressjs module into our application and saved it in a variable called express*/

const express = require("express");

/*
	This creates an application that uses express and store it as an app
		>> app is our server
*/
const app = express();

const port = 4000;

// Middleware - Automatically parses the data skipping the "data" and "end step". Handles the data stringing. 	
	// >>app.use - method of express module where it enables the user to us all functions under "express()" ---> for instance express.json()
app.use(express.json())

// mock database
let users = [
	{
		username: "BMadrigal",
		email: "fateReader@gmail.com",
		password: "dontTalkAboutMe"
	},
	{
		username: "Luisa",
		email: "stronggirl@gmail.com",
		password: "pressure"
	}
];

let items = [
	{
		name: "roses",
		price: 170,
		isActive: true
	},
	{
		name: "tulips",
		price: 250,
		isActive: true
	}
];

// app.get(<endpoint>, <function for req and res>)

app.get("/", (req, res) => {

	// send method --> combination of res.writeHead/status code and res.end
	res.send("Hello from my first expressJS API")

	// res.status(200).send("Hello from my first expressJS API")
	// >> if you want to identify the certain status code
});

/*
	Mini Activity: 5 mins

    >> Create a get route in Expressjs which will be able to send a message in the client:

        >> endpoint: /greeting
        >> message: 'Hello from Batch182-surname'

    >> Test in postman
    >> Send your response in hangouts
*/
// ----> S O L U T I O N <----

app.get("/greeting", (req, res) => {
	res.send("Hello from Batch182-Ferrer")
});


// Retrieval of the mock database
app.get("/users", (req, res) => {

	// stringifies the array
	res.send(users);
});

app.post("/users", (req, res) => {

	console.log(req.body); //result: {}

	// simulate creating a new user document
	let newUser = {
		username: req.body.username,
		email: req.body.email,
		password: req.body.password
	}

	// push newUser into users array
	users.push(newUser);
	console.log(users);

	// send the updated users array in the client
	res.send(users);
});

app.delete("/users", (req, res) => {

	users.pop();
	console.log(users);

	// send the updated users array
	res.send(users);
});

// PUT METHOD

// update user's password
// :index - wildcard ---> specify which user we'll retrieve 
// url: localhost:4000/users/0
app.put("/users/:index", (req, res) => {

	console.log(req.body)

	// an object that contains the value of URL params
	console.log(req.params)

	// parseInt the value of the number coming from req.params
	// ["0"] turns into [0]
	let index = parseInt(req.params.index)

	// users[0].password
	users[index].password = req.body.password

	res.send(users[index]);
});

/*
	Mini-Activity

	>> endpoint: users/update/:index
	>> method: PUT

	>> Update a user's username
	>> Specify the user using the index in the params
	>> Put the updated username in the request body
	>> Send the updated user as a response
	> Test in postman
	>> Send screenshots in hangouts
*/

// ----> S O L U T I O N <----
app.put("/users/updated/:index", (req, res) => {

	let index = parseInt(req.params.index)

	users[index].username = req.body.username;

	res.send(users[index]);
});

// Retrieval of single user
app.get("/users/getSingleUser/:index", (req, res) => {

	console.log(req.params) // result: {index: "1"}

	let index = parseInt(req.params.index)
	// parseInt("1") ---> value above
	console.log(index); // result: 1

	res.send(users[index])
	console.log(users[index]);
})


// ---------------> ACTIVITY 1 SOLUTION < ---------------
// NUMBER 1: GET
app.get("/items", (req, res) => {

	res.send(items);
});

// NUMBER 2: POST/CREATE
app.post("/items", (req, res) => {

	let newItem = {
		name: req.body.name,
		price: req.body.price,
		isActive: req.body.isActive
	}

	items.push(newItem);

	res.send(items);
});

// NUMBER 3: PUT/UPDATE
app.put("/items/:index", (req, res) => {

	let index = parseInt(req.params.index)

	items[index].price = req.body.price;

	res.send(items[index]);
});

// ---------------> END OF ACTIVITY 1 SOLUTION < ---------------

/*
Activity #2
>> Create a new route with '/items/getSingleItem/:index' endpoint. 
    >> This route should allow us to GET the details of a single item.
        -Pass the index number of the item you want to get via the url as url params in your postman.
        -http://localhost:4000/items/getSingleItem/<indexNumberOfItem>
    
    >> Send the request and back to your api
        -get the data from the url params.
        -send the particular item from our array using its index to the client.

>> Create a new route to update an item with '/items/archive/:index' endpoint.    (WITHOUT USING REQUEST BODY)
    >> This route should allow us to UPDATE the isActive property of our product.
        -Pass the index number of the item you want to de-activate via the url as url params.
        -Access the particular item with its index. Access the isActive property of the item and re-assign it to false.
        -send the updated item in the client

>> Create a new route to update an item with '/items/activate/:index' endpoint. (WITHOUT USING REQUEST BODY)
    >> This route should allow us to UPDATE the isActive property of our product.
        -Pass the index number of the item you want to activate via the url as url params.
        -Access the particular item with its index. Access the isActive property of the item and re-assign it to true.
        -send the updated item in the client

>> Create a new collection in Postman called s34-activity
>> Save the Postman collection in your s33-s34 folder

*/

// ---------------> ACTIVITY 2 SOLUTION < ---------------
// NUMBER 1
app.get("/items/getSingleItem/:index", (req, res) => {

	let index = parseInt(req.params.index)

	res.send(items[index])
})

// NUMBER 2
app.put("/items/archive/:index", (req, res) => {

	let index = parseInt(req.params.index)

	items[index].isActive = false

	res.send(items[index]);
});

// NUMBER 3
app.put("/items/activate/:index", (req, res) => {

	let index = parseInt(req.params.index)

	items[index].isActive = true

	res.send(items[index]);
});
// ---------------> END OF ACTIVITY 2 SOLUTION < ---------------


// port
// >> 2 arguments --> port and anonymous function
app.listen(port, () => console.log(`Server is running at port ${port}`))
